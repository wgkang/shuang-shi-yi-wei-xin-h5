<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
        parent::__construct();
        
        $this->load->library('wechat');
        $this->load->library('session');
        $this->load->library('logging');
    }

	public function index()
	{

        $code = $this->input->get('code');
        if ( !empty( $code )){
            $result = $this->wechat->get_access_token($code);

            $openid = $result['openid'];

            $this->session->openid = $openid;
        }


    	$openid = $this->session->openid;
        if(empty($openid)){        	        	
        	// redirect to login
            $baseUrl = 'http://'.$_SERVER['HTTP_HOST'];
            $link = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".WX_APPID."&redirect_uri=".$baseUrl."&response_type=code&scope=snsapi_base#wechat_redirect";
            //$link = 'https://www.danielwellington.cn/wechat/redirect/transpond?appid='.WX_APPID.'&referer=' . urlencode(site_url('user/token'));
            header("location: $link");
            exit;
        }

        $ticket = $this->_get_ticket();
		$data = array(
			'signature' => $this->_jsapi_signature($ticket['jsapi_ticket']),
			'cardList' => $this->_cardList($ticket['api_ticket']),
			'message' => $this->_loveMessage()
		);
        $this->load->view('index', $data);
	}

    private function _get_ticket(){
        $file = APPPATH . 'logs' . DIRECTORY_SEPARATOR . 'ticket';
        // load ticket from file
        $ticket = $this->_get_ticket_from_file($file);
        if(empty($ticket)){
            // load ticket from url
            $content = file_get_contents('http://wechat.danielwellington.cn/get_ticket');
            $ticket = json_decode($content, TRUE);
            if(!$this->_is_valid_ticket($ticket)){
                $ticket = null;
            }
            else{
                // save content to file
                file_put_contents($file, $content);
            }
        }
        return $ticket;
    }

    private function _is_valid_ticket($ticket){
        if(empty($ticket)){
            return false;
        }
        if(!isset($ticket['api_ticket']) || !isset($ticket['jsapi_ticket']) || !isset($ticket['ticket_deadline'])){
            return false;
        }
        return true;
    }

    private function _get_ticket_from_file($file){
        if(!file_exists($file)){
            return null;
        }
        $content = file_get_contents($file);
        $ticket = json_decode($content, TRUE);
        if(empty($ticket)){
            return null;
        }
        if(!isset($ticket['api_ticket']) || !isset($ticket['jsapi_ticket']) || !isset($ticket['ticket_deadline'])){
            return null;
        }
        if($ticket['ticket_deadline'] < time() + 10){
            return null;
        }
        return $ticket;
    }

    private function _jsapi_signature($jsapi){
    	if(empty($jsapi) || $jsapi == 'error'){
            $this->logging->log_error('jsapi is ' . $jsapi);
    		return null;
    	}
    	$nonce = 'Wm3WZYTPz0wzccnWt';
    	$timestamp = time();
    	$url = current_url();
    	if($_SERVER['QUERY_STRING'] != ''){
    		$url.= '?'.$_SERVER['QUERY_STRING'];
    	}
    	$string = 'jsapi_ticket='.$jsapi.'&noncestr='.$nonce.'&timestamp='.time().'&url='.$url;    	
    	return array('nonce' => $nonce, 'timestamp' => $timestamp, 'signature' => sha1($string));
    }

    private function _cardList($ticket){
        if(empty($ticket)){
            $this->logging->log_error('api_ticket is empty');
            return array();
        }

    	$timestamp = time();
    	$nouce = uniqid();

    	$cardId = WX_CARD1;
    	$cardList = array();
    	$string = $timestamp . $ticket . $cardId;
    	$cardList[] = array('timestamp' => $timestamp, 'cardId' => $cardId, 
    		'nouce' => $nouce, 'signature' => sha1($string), 'name' => '满800减200优惠券');

    	$cardId = WX_CARD2;
    	$string = $timestamp . $ticket . $cardId;
    	$cardList[] = array('timestamp' => $timestamp, 'cardId' => $cardId, 
    		'nouce' => $nouce, 'signature' => sha1($string), 'name' => '满1500减300优惠券');
    	return $cardList;
    }

    private function _loveMessage(){
    	$list = array(
			'你知道么，有个人时时想念着你，惦记你<br/>你含笑的眼睛，像星光闪闪<br/>缀在我的心幕上，夜夜亮晶晶',
			'你爱TA，不是因为TA是一个怎样的人<br/>而是因为你喜欢与TA在一起时的感觉',
			'你不知道你是否真的爱<br/>但是你知道你不能没有TA',
			// '想与你经历山川湖海<br/>也想囿于昼夜厨房与爱',
			'爱上你是一见钟情<br/>而爱你是细水长流'
    	);
    	return $list[rand(0, count($list)-1)];
    }

	public function wx_login(){
    	// $link = $this->wechat->get_authorize_url(site_url('user/token'), "123");
        $link = 'https://www.danielwellington.cn/wechat/redirect/transpond?appid='.WX_APPID.'&referer=' . urlencode(site_url('user/token'));
    	header("location: $link");
    }

    public function token(){

    	$code = $this->input->get('code');
    	$result = $this->wechat->get_access_token($code);
    	$access_token = $result['access_token'];
        $openid = $result['openid'];

    	if(!$openid){
    		die('Invalid access');
    	}
    	$this->session->openid = $openid;
    	redirect(site_url('/'));
    }

    public function decrypt(){
    	$code = $this->input->post('code');
    	$access_token = file_get_contents('http://wechat.danielwellington.cn/get_token?token=danielwellington');
		$url = 'https://api.weixin.qq.com/card/code/decrypt?access_token=' . $access_token;
        $postfields = '{"encrypt_code" : "'.$code.'"}';

        $res = $this->wechat->http($url, 'POST', $postfields);
        if(is_array($res) && $res[0] == 200){
        	$data = json_decode($res[1], true);
        	if(is_array($data) && isset($data['code']) && !empty($data['code'])){
        		echo json_encode(array('success' => true, 'code' => $data['code']));
        		exit;
        	}
        }

        echo json_encode(array('success' => false));
        exit;
    }

    public function record($cardId = '', $code = '', $gps = '')
    {
    	$openid = $this->session->openid;
        $this->logging->log_debug('record cardId=' . $cardId . ' code=' . $code . ' gps='.$gps . ' openid=' . $openid);
        if(empty($openid)){  
            $this->logging->log_error('record no openid. cardId='.$cardId);
        	echo 'Invalid access';
        	exit;
        }

		$cardId = urldecode($cardId);
		$code = urldecode($code);
		$gps = urldecode($gps);
        $this->load->model('user_model');
        $res = $this->user_model->addItem($openid, $cardId, $code, $gps);
        $this->logging->log_debug('record added for ' . $openid);
        echo $res ? 'success' : 'failure';
        exit;
    }

    public function test()
    {
        $ticket = $this->_get_ticket();
        $data = array(
            'signature' => $this->_jsapi_signature($ticket['jsapi_ticket']), 
            'cardList' => $this->_cardList($ticket['api_ticket']),
            'message' => $this->_loveMessage()
        );
        $this->load->view('index', $data);        
    }
}
