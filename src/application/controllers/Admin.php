<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
    	if(!$this->session->admin){
    		$this->load->view('admin/login', array('error' => NULL));
    		return;
    	}
    	$this->load->view('admin/index');
    }


    public function login()
    {
    	$token = $this->input->post('token');
    	if($token != NULL && $token == getenv('ADMIN_TOKEN')){
    		$this->session->admin = TRUE;
    		redirect(site_url('admin'));
    	}
    	$this->load->view('admin/login', array('error' => 'Invalid token.'));
    }

    public function report($date = NULL)
    {
    	if(!$this->session->admin){
    		$this->load->view('admin/login', array('error' => NULL));
    		return;
    	}

    	$list = $this->getData($date);
    	$this->load->view('admin/report', array('date' => $date, 'list' => $list));
    }

    public function getOne(){
        $data = $this->getData(date("Y-m-d",strtotime("-1 day")));
        echo $this->excelData($data,date("Y-m-d",strtotime("-1 day")));
    }
    public function getAll(){
        $data = $this->getData(null);
        echo $this->excelData($data,date("Y-m-d h:i:s",time()));
    }

    public function sendEmail(){
	    //2个表，一个输出总计，一个输出当天
        $_date  =  date("Hi",time());
        if ($_date!='1000')exit('error');

	    $post_data['type'] = 'email';
	    $post_data['to'] = 'melody.liu@danielwellington.com,dee.li@danielwellington.com,susie.huang@danielwellington.com,Leo.wang@danielwellington.com,hades.deng@danielwellington.com,zhen.jiang@danielwellington.com,zhuwei.chen@danielwellington.com,nina.huang@danielwellington.com';
	    $post_data['subject'] = date("Y-m-d",strtotime("-1 day")).'数据统计';
	    $post_data['text'] = '以上是昨天的数据以及到目前截止的所有领券数据统计，请查收。';
	    $post_data['attachments'] = [
	        [
                'filename'=>date("Y-m-d",strtotime("-1 day")).'.xls',
                'path'=>base_url().'admin/getone'
            ],
            [
                'filename'=>'total.xls',
                'path'=>base_url().'admin/getall'
            ]
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, 'https://api-internal.danielwellington.com/notification-service/sendMessage');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['x-api-key: '.API_KEY]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));

        $response = curl_exec($ch);
        curl_close($ch);

        exit($response);

    }

    /*

    *处理Excel导出

    *@param $datas array 设置表格数据

    *@param $titlename string 设置head

    *@param $title string 设置表头

    */

    public function excelData($datas,$date){

        $str = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\nxmlns:x=\"urn:schemas-microsoft-com:office:excel\"\r\nxmlns=\"http://www.w3.org/TR/REC-html40\">\r\n<head>\r\n<meta http-equiv=Content-Type content=\"text/html; charset=utf-8\">\r\n</head>\r\n<body>";

        $str .="<table border=1><head><tr> 

               <th style='width:300px;' >截止日期</th> 
               
               <th style='width:300px;' >门店</th> 

               <th style='width:70px;' >领券</th> 

           </tr></head>";


        foreach ($datas as $key=> $rt )

        {

            $str .= "<tr><td>{$date}</td>";

            foreach ( $rt as $k => $v )

            {

                $str .= "<td>{$v}</td>";

            }

            $str .= "</tr>\n";

        }

        $str .= "</table></body></html>";

        header( "Content-Type: application/vnd.ms-excel; name='excel'" );

        header( "Content-type: application/octet-stream" );

        header( "Content-Disposition: attachment; filename=".date("Y-m-d",strtotime("-1 day")).'.xls' );

        header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );

        header( "Pragma: no-cache" );

        header( "Expires: 0" );

        return $str;

    }

    private function getData($date){
        $this->load->model('user_model');
        $locations = $this->user_model->listLocationsByDate($date);
        $stores = $this->_storeList();
        $found = array();
        $others = 0;
        $total = 0;
        foreach($locations as $loc){
            $parts = explode(',', $loc);
            $lat = $parts[0];
            $lon = $parts[1];

            $minName = NULL;
            $minDist = 100000;
            foreach($stores as $store){
                $dist = $this->_distance($lat, $lon, $store['lat'], $store['lon']);
                if($dist < $minDist){
                    $minDist = $dist;
                    $minName = $store['name'];
                }
            }
            if($minDist < 1){
                if(isset($found[$minName])){
                    $found[$minName] ++;
                }
                else{
                    $found[$minName] = 1;
                }
            }
            else{
                $others ++;
            }
            $total ++;
        }

        $list = array();
        foreach($found as $name => $count){
            $list[] = array('name' => $name, 'count' => $count);
        }
        usort($list, array($this, '_storeCmp'));
        $list[] = array('name' => '其他', 'count' => $others);
        $list[] = array('name' => '总数', 'count' => $total);
        return $list;
    }

    private function _storeCmp($s1, $s2)
    {
    	if($s1['count'] > $s2['count']){
    		return -1;
    	}
    	if($s1['count'] < $s2['count']){
    		return 1;
    	}
    	return 0;
    }

    private function _distance($lat1, $lon1, $lat2, $lon2)
    {
    	$earthRadiusKm = 6371.0;

		$dLat = $this->_toRadian($lat2-$lat1);
		$dLon = $this->_toRadian($lon2-$lon1);

		$lat1 = $this->_toRadian($lat1);
		$lat2 = $this->_toRadian($lat2);

		$a = sin($dLat/2) * sin($dLat/2) +
		     sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
		return $earthRadiusKm * $c;
    }

    private function _toRadian($degree)
    {
    	return $degree / 180.0 * M_PI;
    }

    private function _storeList()
    {
    	$list = array();
		$list[] = array('name' => '珠海华发商都', 'address' => '广东省珠海市珠海大道8号华发商都2号楼1层B10B11号铺', 'lat' => 22.224603, 'lon' => 113.510028);
		$list[] = array('name' => '镇江万达广场', 'address' => '江苏省镇江市润州区黄山西路19号万达广场1号门2层2002号DW精品店', 'lat' => 32.195576, 'lon' => 119.436573);
		$list[] = array('name' => '漳州凯德广场', 'address' => '福建省漳州市芗城区南昌路49号漳州凯德广场1层DW精品店', 'lat' => 24.510388, 'lon' => 117.6550235);
		$list[] = array('name' => '张家港曼巴特', 'address' => '江苏省张家港市区河西路88号曼巴特购物广场2层2239号DW精品店', 'lat' => 31.860567, 'lon' => 120.544836);
		$list[] = array('name' => '宜昌万达广场', 'address' => '湖北省宜昌市伍家岗区沿江大道特166号万达广场室内步行街1层DW精品店', 'lat' => 30.680622, 'lon' => 111.306578);
		$list[] = array('name' => '扬州京华金鹰', 'address' => '江苏省扬州市邗江区京华东路168号金鹰国际购物中心1层32号DW精品店', 'lat' => 32.385495, 'lon' => 119.374525);
		$list[] = array('name' => '扬州邗江万达', 'address' => '江苏省扬州市邗江区邗江中路302号邗江万达广场2号门1层DW精品店', 'lat' => 32.3721291, 'lon' => 119.4004086);
		$list[] = array('name' => '扬州文昌金鹰', 'address' => '江苏省扬州市广陵区汶河南路120号金鹰国际购物中心1层DW精品店', 'lat' => 32.393805, 'lon' => 119.432809);
		$list[] = array('name' => '徐州苏宁广场', 'address' => '江苏省徐州市淮海东路29号苏宁广场4层DW手表精品店', 'lat' => 34.265618, 'lon' => 117.188467);
		$list[] = array('name' => '咸阳万达', 'address' => '陕西省咸阳市秦都区玉泉东路10号咸阳丽彩万达1层DW精品店', 'lat' => 34.3418815, 'lon' => 108.6934242);
		$list[] = array('name' => '湘潭万达广场', 'address' => '湖南省湘潭市高新区芙蓉中路281号万达广场1层DW精品店', 'lat' => 27.8247531, 'lon' => 112.926859);
		$list[] = array('name' => '西安群光广场', 'address' => '陕西省西安市新城区南新街群光广场1层1B22号DW精品店', 'lat' => 34.261474, 'lon' => 108.954631);
		$list[] = array('name' => '西安大明宫万达', 'address' => '陕西省西安市未央区太华北路369号大明宫万达1层DW精品店', 'lat' => 34.316558, 'lon' => 108.972817);
		$list[] = array('name' => '西安金地广场', 'address' => '陕西省西安市雁塔区曲江池东路金地广场1层DW精品店', 'lat' => 34.204758, 'lon' => 108.988147);
		$list[] = array('name' => '西安明乐园万达', 'address' => '陕西省西安市新城区解放路111号，明乐园万达广场1层DW精品店', 'lat' => 34.264337, 'lon' => 108.9628989);
		$list[] = array('name' => '西安咸阳万象城', 'address' => ':陕西省西安市未央区三桥新街1076号西咸万象城1层DW精品店', 'lat' => 34.294173, 'lon' => 108.948266);
		$list[] = array('name' => '厦门SM广场', 'address' => '福建省厦门市湖里区嘉禾路468号SM城市广场一期1层K113号DW手表精品店', 'lat' => 24.501002, 'lon' => 118.126884);
		$list[] = array('name' => '厦门湖里万达', 'address' => '福建省厦门市湖里区仙岳路4666号厦门湖里万达广场2层277BB号DW精品店', 'lat' => 24.504898, 'lon' => 118.177462);
		$list[] = array('name' => '厦门中闽百汇梧村店', 'address' => '福建省厦门市思明区厦禾路929号中闽百汇商场B1层DW精品店', 'lat' => 24.470967, 'lon' => 118.116565);
		$list[] = array('name' => '无锡荟聚', 'address' => '江苏省无锡市锡山区团结中路3号荟聚购物中心2层DW手表精品店', 'lat' => 31.6166857, 'lon' => 120.4008906);
		$list[] = array('name' => '无锡滨湖万达', 'address' => '江苏省无锡市滨湖区河埒口梁溪路35号滨湖万达2层70号DW精品店', 'lat' => 31.5627415, 'lon' => 120.2665476);
		$list[] = array('name' => '无锡万象城', 'address' => '江苏省无锡市滨湖区金石路88号万象城1层DW精品店', 'lat' => 31.513892, 'lon' => 120.283928);
		$list[] = array('name' => '安徽芜湖银泰', 'address' => '安徽省芜湖市戈江区利民西路189号芜湖银泰城1层DW精品店', 'lat' => 31.311188, 'lon' => 118.367333);
		$list[] = array('name' => '武汉荟聚', 'address' => '湖北省武汉市硚口区园博大道1号荟聚购物中心2层WH-KSK-08号DW精品店', 'lat' => 30.630582, 'lon' => 114.215623);
		$list[] = array('name' => '武汉菱角湖万达', 'address' => '湖北省武汉市江汉区唐家墩路5号菱角湖万达广场1层DW精品店', 'lat' => 30.610273, 'lon' => 114.268696);
		$list[] = array('name' => '武汉武胜凯德广场', 'address' => '湖北省武汉市硚口区中山大道238号武胜路凯德广场1层01-47号DW精品店', 'lat' => 30.568751, 'lon' => 114.267609);
		$list[] = array('name' => '武汉K11', 'address' => '湖北省武汉市洪山区关山大道355号武汉K11艺术购物中心1层DW精品店', 'lat' => 30.4929364, 'lon' => 114.4115404);
		$list[] = array('name' => '温州万象城', 'address' => '浙江省温州市瓯海区温瑞大道999号华润万象城地B1层B152b号DW精品店', 'lat' => 27.928552, 'lon' => 120.6859386);
		$list[] = array('name' => '温州龙湾万达广场', 'address' => '浙江省温州市龙湾区永定路1188号万达广场1层0003号DW精品店', 'lat' => 27.920399, 'lon' => 120.811641);
		$list[] = array('name' => '天津滨海万达', 'address' => '天津市滨海新区紫云公园西侧万达广场1层DW精品店', 'lat' => 39.0125337, 'lon' => 117.6898702);
		$list[] = array('name' => '唐山百货大楼', 'address' => '河北省唐山市路北区新华东道125号百货大楼1层DW精品店', 'lat' => 39.626051, 'lon' => 118.180093);
		$list[] = array('name' => '苏州印象城', 'address' => '江苏省苏州市工业园区现代大道1699号印象城2层DW精品店', 'lat' => 31.318382, 'lon' => 120.656438);
		$list[] = array('name' => '苏州天虹', 'address' => '江苏省苏州市工业园区苏雅路388号天虹百货A座1层A1-3号DW手表精品店', 'lat' => 31.316207, 'lon' => 120.664777);
		$list[] = array('name' => '苏州泰华商城', 'address' => '江苏省苏州市人民路383号泰华商场二层DW手表精品店', 'lat' => 31.290117, 'lon' => 120.624598);
		$list[] = array('name' => '苏州狮山龙湖', 'address' => '江苏省苏州市虎丘区金山路81号1层36号DW手表精品店', 'lat' => 31.2948797, 'lon' => 120.5401452);
		$list[] = array('name' => '苏州观前美罗商城', 'address' => '江苏省苏州市姑苏区人民路观前街245号美罗商城2层DW手表精品店', 'lat' => 31.310151, 'lon' => 120.622075);
		$list[] = array('name' => '苏体广场', 'address' => '江苏省苏州市吴中区中新大道东999号苏州奥林匹克体育中心B1-26 DW精品店', 'lat' => 31.3087712, 'lon' => 120.7506251);
		$list[] = array('name' => '深圳金光华', 'address' => '广东省深圳市罗湖区人民南路2028号金光华广场B1层072A号DW精品店', 'lat' => 22.539533, 'lon' => 114.120133);
		$list[] = array('name' => '深圳壹方城', 'address' => '广东省深圳市宝安区新安街道新湖路99号壹方城1层DW精品店', 'lat' => 22.5633865, 'lon' => 113.8775282);
		$list[] = array('name' => '深圳太阳百货', 'address' => '广东省深圳市罗湖区解放路2001东门老街太阳百货2层DW精品店', 'lat' => 22.54532, 'lon' => 114.118505);
		$list[] = array('name' => '深圳龙华九方购物广场', 'address' => '广东省深圳市宝安区龙华新区民治街道人民路2020号龙华九方购物中心1层DW精品店', 'lat' => 22.627714, 'lon' => 114.021274);
		$list[] = array('name' => '深圳来福士', 'address' => '广东省深圳市南山区南海大道2163号来福士商场B1层K09号DW精品店', 'lat' => 22.5113029, 'lon' => 113.925766);
		$list[] = array('name' => '深圳COCO PARK', 'address' => '广东省深圳市福田区福华三路星河CocoparkB1层DW精品店', 'lat' => 22.53323, 'lon' => 114.054448);
		$list[] = array('name' => '深圳华强北九方', 'address' => '广东省深圳市福田区中航路华强九方购物中心B1层B175DW精品店', 'lat' => 22.543373, 'lon' => 114.082998);
		$list[] = array('name' => '深圳怡景中心城', 'address' => '广东省深圳市福田区福华路怡景中心城G层G206 DW精品店', 'lat' => 22.536587, 'lon' => 114.059355);
		$list[] = array('name' => '深圳华润1234', 'address' => '广东省深圳市罗湖区建设路1234华润万象2层L202 DW精品店', 'lat' => 22.539697, 'lon' => 114.110883);
		$list[] = array('name' => '深圳新城市广场', 'address' => '广东省深圳市福田区新城市广场LG层中岛003号DW精品店', 'lat' => 22.539291, 'lon' => 114.098419);
		$list[] = array('name' => '上海港汇名城', 'address' => '上海市徐汇区虹桥路1号港汇广场B1层名店运动城内DW精品店', 'lat' => 31.1944205, 'lon' => 121.4372385);
		$list[] = array('name' => '上海正大广场', 'address' => '上海市浦东新区陆家嘴西路168号2层DW精品店', 'lat' => 31.236604, 'lon' => 121.500444);
		$list[] = array('name' => '上海淮海百盛', 'address' => '上海市黄浦区淮海中路918号3层DW精品店', 'lat' => 31.217238, 'lon' => 121.459757);
		$list[] = array('name' => '上海大悦城', 'address' => '上海市静安区西藏北路166号上海大悦城5层S501-18号DW精品店', 'lat' => 31.2429229, 'lon' => 121.4722694);
		$list[] = array('name' => '上海周浦万达', 'address' => '上海市浦东新区周浦镇年家浜路518号1层0035号DW精品店', 'lat' => 31.113617, 'lon' => 121.567791);
		$list[] = array('name' => '上海龙之梦莘庄', 'address' => '上海市闵行区沪闵路6088号龙之梦购物中心1层01-K01号DW精品店', 'lat' => 31.1386529, 'lon' => 121.4094618);
		$list[] = array('name' => '上海环球港', 'address' => '上海市普陀区中山北路3300号上海环球港1层L1021号DW精品店', 'lat' => 31.2334421, 'lon' => 121.412581);
		$list[] = array('name' => '上海五角万达', 'address' => '上海市杨浦区邯郸路600号五角场万达B1层0079号DW精品店', 'lat' => 31.3000655, 'lon' => 121.5139827);
		$list[] = array('name' => '上海新世界城', 'address' => '上海市黄浦区南京西路2号上海新世界城3层DW精品店', 'lat' => 31.2348549, 'lon' => 121.4736559);
		$list[] = array('name' => '上海龙之梦中山公园店', 'address' => '上海市长宁区长宁路1018号中山公园龙之梦B区B1层黄中庭DW精品店 ', 'lat' => 31.219051, 'lon' => 121.416591);
		$list[] = array('name' => '上海万象城', 'address' => '上海市吴中路1799号万象城2层L261号DW精品店', 'lat' => 31.1690249, 'lon' => 121.3632932);
		$list[] = array('name' => '上海金桥国际广场', 'address' => '上海市浦东新区张杨路3611号金桥国际广场1座1楼DW精品店', 'lat' => 31.256152, 'lon' => 121.579937);
		$list[] = array('name' => '上海莘庄仲盛店', 'address' => '上海市闵行区都市路5001号仲盛世界商城2层-60-7DW精品店', 'lat' => 31.107263, 'lon' => 121.386886);
		$list[] = array('name' => '泉州中骏世界城', 'address' => '福建省泉州市丰泽区安吉南路69号中骏世界城1层DW精品店', 'lat' => 24.9236088, 'lon' => 118.6616592);
		$list[] = array('name' => '宁波印象城', 'address' => '浙江省宁波市鄞州区钱湖北路288号印象城B1层09B号DW精品店', 'lat' => 29.83312, 'lon' => 121.56733);
		$list[] = array('name' => '宁波东门银泰', 'address' => '浙江省宁波市海曙区中山东路238号东门银泰1层dw手表精品店', 'lat' => 29.854579, 'lon' => 121.621646);
		$list[] = array('name' => '南通金鹰圆融店', 'address' => '江苏省南通市崇川区工农路57号金鹰圆融购物中心1层DW精品店', 'lat' => 32.004002, 'lon' => 120.883833);
		$list[] = array('name' => '南京吾悦广场', 'address' => '江苏省南京市鼓层区湖南路街道湖北路51号南京吾悦1层1020号DW精品店', 'lat' => 32.0674644, 'lon' => 118.7768278);
		$list[] = array('name' => '南京建邺万达', 'address' => '江苏省南京市建邺区江东中路98号万达广场D入口1层L138号DW精品店', 'lat' => 32.033048, 'lon' => 118.736027);
		$list[] = array('name' => '南京新百', 'address' => '江苏省南京市中山南路18号南京新百B座1层DW精品店', 'lat' => 32.040086, 'lon' => 118.78524);
		$list[] = array('name' => '南京弘阳广场', 'address' => '江苏省南京市浦口区大桥北路48号弘阳广场1层DW精品店', 'lat' => 32.1351034, 'lon' => 118.7273929);
		$list[] = array('name' => '南京仙林金鹰和谐广场', 'address' => '江苏省南京市栖霞区学金路仙林金鹰二期1层DW柜台', 'lat' => 32.1008476, 'lon' => 118.9274138);
		$list[] = array('name' => '南昌红谷滩', 'address' => '江西省南昌市青山湖区会展路999号红谷滩2层DW精品店', 'lat' => 28.693733, 'lon' => 115.850118);
		$list[] = array('name' => '昆山百盛', 'address' => '江苏省昆山市前进西路300号百盛购物中心1层68号DW手表精品店', 'lat' => 31.3821084, 'lon' => 120.9549189);
		$list[] = array('name' => '昆山金鹰', 'address' => '江苏省苏州市昆山市玉山镇珠江中路昆山金鹰2层A53号DW手表柜台', 'lat' => 31.376542, 'lon' => 120.973548);
		$list[] = array('name' => '昆山九方', 'address' => '江苏省苏州（昆山市）萧林中路666号九方购物中心1层DW精品店', 'lat' => 31.4041576, 'lon' => 120.9537237);
		$list[] = array('name' => '昆明新西南百盛', 'address' => '云南省昆明市盘龙区人民中路17号新西南广场(近小花园)百盛购物中心2层DW精品店', 'lat' => 25.0420099, 'lon' => 102.7145355);
		$list[] = array('name' => '昆明顺城购物中心', 'address' => '云南省昆明市五华区东风西路11号昆明顺城购物中心B1层DW精品店', 'lat' => 25.0375058, 'lon' => 102.7131684);
		$list[] = array('name' => '昆明柏联百盛', 'address' => '云南省昆明市五华区三市街6号柏联百盛2层DW精品店', 'lat' => 25.035186, 'lon' => 102.711057);
		$list[] = array('name' => '九江九方', 'address' => '江西省九江市庐山区八里湖新区长虹西大道101号九方购物中心1层LA113号DW精品店', 'lat' => 29.6857168, 'lon' => 115.9790718);
		$list[] = array('name' => '金华江北一百', 'address' => '浙江省金华市婺城区西市街159号2层DW精品店', 'lat' => 29.102198, 'lon' => 119.65388);
		$list[] = array('name' => '金华万达广场', 'address' => '浙江省金华市金东区李渔东路300号万达广场1层1005A号DW精品店', 'lat' => 29.090399, 'lon' => 119.678045);
		$list[] = array('name' => '江门汇悦城', 'address' => '广东省江门市白石大道166号汇悦城购物中心1层1F062号铺DW精品店', 'lat' => 22.611471, 'lon' => 113.083407);
		$list[] = array('name' => '吉安天虹', 'address' => '江西省吉安市吉州区天虹购物中心1层DW精品店', 'lat' => 27.114134, 'lon' => 114.984285);
		$list[] = array('name' => '江苏淮安万达', 'address' => '江苏省淮安市清江浦区翔宇中路169号万达广场1层DW精品店', 'lat' => 33.456052, 'lon' => 119.407158);
		$list[] = array('name' => '合肥包河万达广场', 'address' => '安徽省合肥市马鞍山路130号万达百货1层DW手表精品店', 'lat' => 31.8572392, 'lon' => 117.3020454);
		$list[] = array('name' => '合肥悦方mall', 'address' => '安徽省合肥市滨湖新区庐州大道888号悦方Mall 1层DW精品店', 'lat' => 31.73511, 'lon' => 117.303554);
		$list[] = array('name' => '哈尔滨麦凯乐商场', 'address' => '黑龙江省哈尔滨市道里区尚志大街73号哈尔滨麦凯乐店2层DW精品店', 'lat' => 45.771304, 'lon' => 126.623653);
		$list[] = array('name' => '哈尔滨松雷购物中心', 'address' => '黑龙江省哈尔滨市南岗区东大直街329号松雷南岗中心店2层DW精品店', 'lat' => 45.759177, 'lon' => 126.644209);
		$list[] = array('name' => '哈尔滨万达茂', 'address' => '黑龙江省哈尔滨市松北区世茂大道99号万达茂购物中心2层0014号DW精品店', 'lat' => 45.8043042, 'lon' => 126.5115154);
		$list[] = array('name' => '杭州庆春银泰', 'address' => '浙江省杭州市江干区景昙路18-26号庆春银泰2层DW精品店', 'lat' => 30.260027, 'lon' => 120.205192);
		$list[] = array('name' => '杭州龙湖天街', 'address' => '浙江省杭州市江干区金沙大道560号龙湖金沙天街1层DW精品店', 'lat' => 30.310544, 'lon' => 120.327052);
		$list[] = array('name' => '杭州水晶城', 'address' => '浙江省杭州市拱墅区上塘路458号水晶购物城1层1301号DW精品店', 'lat' => 30.303873, 'lon' => 120.152307);
		$list[] = array('name' => '杭州湖滨银泰C区', 'address' => '浙江省杭州市上城区延安路258号湖滨银泰C区2层DW手表精品店', 'lat' => 30.2531544, 'lon' => 120.162268);
		$list[] = array('name' => '杭州西湖银泰', 'address' => '浙江省杭州市上城区延安路98号西湖银泰城B区1层1C-108号DW精品店', 'lat' => 30.243966, 'lon' => 120.164828);
		$list[] = array('name' => '杭州武林银泰', 'address' => '浙江省杭州市下城区延安路530号武林银泰百货2层DW精品店', 'lat' => 30.243966, 'lon' => 120.164828);
		$list[] = array('name' => '杭州西溪印象城', 'address' => '浙江省杭州市余杭区五常大道1号西溪印象城2层P1-02-K03号DW精品店', 'lat' => 30.247353, 'lon' => 120.051385);
		$list[] = array('name' => '杭州城西银泰城', 'address' => '浙江省杭州市拱墅区萍水街丰潭路380号城西银泰1层DW精品店', 'lat' => 30.300247, 'lon' => 120.108663);
		$list[] = array('name' => '杭州湖滨银泰B区', 'address' => '浙江省杭州市延安路243号杭州湖滨银泰B区B1层DW精品店', 'lat' => 30.2542538, 'lon' => 120.1640438);
		$list[] = array('name' => '海宁银泰城', 'address' => '浙江省海宁市海昌南路363号海宁银泰城1层DZ-S104号DW精品店', 'lat' => 30.501207, 'lon' => 120.692345);
		$list[] = array('name' => '贵阳鸿通城', 'address' => '贵州省贵阳市南明区解放路鸿通城100号购物中心1层M04-3号DW精品店', 'lat' => 26.55844, 'lon' => 106.708883);
		$list[] = array('name' => '贵阳世纪金源', 'address' => '贵州省贵阳市观山湖区北京西路64号世纪金源购物中心B1层BF1Z001号DW精品店', 'lat' => 26.5985347, 'lon' => 106.6267652);
		$list[] = array('name' => '广州天河百货', 'address' => '广东省广州市越秀区北京路一街238号明盛广场天河城百货1层DW精品店', 'lat' => 23.123024, 'lon' => 113.269326);
		$list[] = array('name' => '广州白云万达', 'address' => '广东省广州市白云区城东路509万达广场1层1177号DW精品店', 'lat' => 23.171207, 'lon' => 113.267311);
		$list[] = array('name' => '广州正佳广场', 'address' => '广东省广州市天河区天河路228号正佳广场2层2G040号DW精品店', 'lat' => 23.132146, 'lon' => 113.326998);
		$list[] = array('name' => '广州永旺梦乐城', 'address' => '广东省广州市番禺区大龙街亚运大道1号永旺梦乐城B1层DW精品店', 'lat' => 22.9389903, 'lon' => 113.4435568);
		$list[] = array('name' => '广州西城都荟', 'address' => '广东省广州市荔湾区黄沙大道8号广州西城都荟B1层DW精品店', 'lat' => 23.109291, 'lon' => 113.240227);
		$list[] = array('name' => '广州番禺奥园', 'address' => '广东省广州市番禺区桥南街福德路281、299、315号 奥园广场 1层DW精品店', 'lat' => 22.923709, 'lon' => 113.357475);
		$list[] = array('name' => '江西赣州九方', 'address' => '江西省赣州市章贡区长征大道1号九方购物中心2层DW精品店', 'lat' => 25.836466, 'lon' => 114.935077);
		$list[] = array('name' => '福州仓山万达', 'address' => '福建省福州市仓山区浦上大道276号仓山万达广场1层DW精品店', 'lat' => 26.0358509, 'lon' => 119.2757818);
		$list[] = array('name' => '福州东街口东百中心', 'address' => '福建省福州市鼓层区八一七北路88号东百大厦2层DW精品店', 'lat' => 26.0854756, 'lon' => 119.2991051);
		$list[] = array('name' => '福州金融街万达', 'address' => '福建省福州市台江区鳌江路8号金融街万达广场1层DW手表店', 'lat' => 26.05244, 'lon' => 119.342644);
		$list[] = array('name' => '福州五四北泰禾广场', 'address' => '福建省福州市晋安区秀峰路423号五四北泰禾广场1层DW精品店', 'lat' => 26.1394982, 'lon' => 119.3265219);
		$list[] = array('name' => '佛山岭南新天地', 'address' => '广东省佛山市禅城区岭南新天地福晨里FC206号铺', 'lat' => 23.029824, 'lon' => 113.115466);
		$list[] = array('name' => '东莞汇一城', 'address' => '广东省东莞市南城区第一国际汇一城1号裙楼1层B1107号DW精品', 'lat' => 23.020673, 'lon' => 113.751799);
		$list[] = array('name' => '大连柏威年购物中心', 'address' => '辽宁省大连市中山区柏威年购物广场地B1层B1-KG001b号DW精品店', 'lat' => 38.91791, 'lon' => 121.627066);
		$list[] = array('name' => '大连凯德和平广场店', 'address' => '辽宁省大连市沙河口区高尔基路695号凯德和平广场1层01-K06 DW商铺', 'lat' => 38.8966518, 'lon' => 121.5881926);
		$list[] = array('name' => '重庆北城天街', 'address' => '重庆市江北区观音桥北城天街10号北城天街购物广场B1层45号dw精品店', 'lat' => 29.578754, 'lon' => 106.533973);
		$list[] = array('name' => '重庆杨家坪大洋百货', 'address' => '重庆市九龙坡区杨家坪西郊路3号(轻轨车站旁)大洋百货2层DW精品店', 'lat' => 29.507249, 'lon' => 106.5140262);
		$list[] = array('name' => '重庆西城天街', 'address' => '重庆市九龙坡区珠江路48号西城天街购物广场L馆1层C13号DW精品店', 'lat' => 29.5109, 'lon' => 106.516938);
		$list[] = array('name' => '重庆南坪万达', 'address' => '重庆市南岸区南城大道8号万达广场室内步行街LG层64号DW精品店', 'lat' => 29.526656, 'lon' => 106.567948);
		$list[] = array('name' => '重庆U城天街', 'address' => '重庆市沙坪坝区大学城北路97号重庆龙湖U城天街B馆LG层DW精品店', 'lat' => 29.6073132, 'lon' => 106.3161531);
		$list[] = array('name' => '成都群光广场', 'address' => '四川省成都市锦江区春熙路南段8号群光广场2层DW精品店', 'lat' => 30.6537099, 'lon' => 104.0764816);
		$list[] = array('name' => '成都金牛凯德', 'address' => '四川省成都市金牛区交大路183号凯德广场一期1层1-77号DW精品店', 'lat' => 30.703118, 'lon' => 104.043836);
		$list[] = array('name' => '成都盐市口新世界百货', 'address' => '四川省成都市锦江区盐市口顺城大街8号新世界百货2层2600251号DW精品店', 'lat' => 30.656468, 'lon' => 104.071439);
		$list[] = array('name' => '成都双流海滨城', 'address' => '四川省成都市双流区蛟龙大道21号海滨城购物中心1层A168-1号DW精品店', 'lat' => 30.603562, 'lon' => 103.910605);
		$list[] = array('name' => '成都大悦城', 'address' => '四川省成都市武侯区大悦路518号大悦城1层DW精品店', 'lat' => 30.626443, 'lon' => 104.01037);
		$list[] = array('name' => '成都蜀都万达', 'address' => '四川省成都市郫县区郫筒街道望丛东路139号蜀都万达广场1层DW精品店', 'lat' => 30.808326, 'lon' => 103.906191);
		$list[] = array('name' => '成都太古里', 'address' => ' 四川省成都市锦江区中纱帽街8号远洋太古里 B1层DW精品店', 'lat' => 30.651959, 'lon' => 104.083253);
		$list[] = array('name' => '常州百货', 'address' => '江苏省常州市钟层区莱蒙商业街4号百货大楼1层B135-1号DW手表精品店', 'lat' => 31.811226, 'lon' => 119.974061);
		$list[] = array('name' => '常州吾悦国际广场', 'address' => '江苏省常州市钟层区延陵西路123号吾悦国际广场1层1号DW手表精品店', 'lat' => 31.7811165, 'lon' => 119.9479141);
		$list[] = array('name' => '常州新北万达', 'address' => '江苏省常州市新北区通江中路88号万达广场2号门1层0003号DW精品店', 'lat' => 31.819474, 'lon' => 119.971064);
		$list[] = array('name' => '长沙德思勤', 'address' => '湖南省长沙市雨花区湘府中路18号德思勤城市广场1层72号DW精品店', 'lat' => 28.111052, 'lon' => 113.014126);
		$list[] = array('name' => '长沙叮叮Mall', 'address' => '湖南省长沙市天心区劳动西路337号叮叮商业广场B1层141号DW精品店', 'lat' => 28.1737429, 'lon' => 112.9824092);
		$list[] = array('name' => '长沙国金街', 'address' => '湖南省长沙市芙蓉区黄兴中路78号长沙国金街DW精品店', 'lat' => 28.196303, 'lon' => 112.976082);
		$list[] = array('name' => '长沙泊富广场', 'address' => '湖南省长沙市开福区芙蓉中路与湘春路交汇处泊富CITY美好生活中心1层DW精品店', 'lat' => 28.224312, 'lon' => 112.988284);
		$list[] = array('name' => '长沙梅溪湖步步高新天地', 'address' => '湖南省长沙市岳麓区东方红路657号梅溪湖步步高新天地G层DW精品店', 'lat' => 28.19779, 'lon' => 112.863487);
		$list[] = array('name' => '北京西单大悦城', 'address' => '北京市西城区西单北大街131号西单大悦城3层D015号DW精品店', 'lat' => 39.910278, 'lon' => 116.3725);
		$list[] = array('name' => '北京汉光百货', 'address' => '北京市西城区西单北大街176号汉光百货2层DW精品店', 'lat' => 39.907778, 'lon' => 116.374444);
		$list[] = array('name' => '北京翠微商场', 'address' => '北京市海淀区33号翠微广场B座2层DW精品店', 'lat' => 39.981709, 'lon' => 116.317043);
		$list[] = array('name' => '北京国瑞城', 'address' => '北京市东城区崇文门外大街18号国瑞城购物中心1层FZID-10号DW精品店', 'lat' => 39.8971309, 'lon' => 116.4185189);
		$list[] = array('name' => '北京新中关购物中心', 'address' => '北京市海淀区中关村大街19号新中关购物中心1层KL106号DW精品店', 'lat' => 39.978328, 'lon' => 116.315344);
		$list[] = array('name' => '北京蓝色港湾', 'address' => '北京市朝阳区蓝色港湾1号层M层SMM-K09 DW精品店', 'lat' => 39.9430504, 'lon' => 116.4744001);
		$list[] = array('name' => '北京东直门银座购物中心', 'address' => '北京市东城区东直门外大街48号东方银座商场L1层K11', 'lat' => 39.940001, 'lon' => 116.435462);
		$list[] = array('name' => '北京崇文门新世界', 'address' => '北京市东城区崇文门新世界2期1层DW精品店', 'lat' => 39.898464, 'lon' => 116.418105);
		$list[] = array('name' => '厦门来雅百货', 'address' => '福建省厦门市思明区中山路193-215号来雅一期1层DW精品店', 'lat' => 24.454398, 'lon' => 118.081324);
		$list[] = array('name' => '广州嘉裕太阳城', 'address' => '广东省广州市白云区广州大道北1811号嘉裕太阳城广场一层DW精品店', 'lat' => 23.18757, 'lon' => 113.327473);
		$list[] = array('name' => '上海宝山龙湖天街POP', 'address' => '上海市宝山区丹霞山路50弄1号龙湖宝山天街1层DW精品店', 'lat' => 31.3612321, 'lon' => 121.3619013);
		$list[] = array('name' => '安徽芜湖镜湖万达POP', 'address' => '安徽省芜湖市鸠江区赭山东路1号万达广场1层DW精品店', 'lat' => 31.338459, 'lon' => 118.400141);
		$list[] = array('name' => '上海青浦吾悦POP', 'address' => '上海市青浦区淀山湖大道218号青浦吾悦广场1层DW快闪店', 'lat' => 31.144431, 'lon' => 121.095007);
		$list[] = array('name' => '湛江万达', 'address' => ' 广东省湛江市经济技术开发区海滨大道128号万达广场首层 DW精品店', 'lat' => 21.232548, 'lon' => 110.4139939);
		$list[] = array('name' => '江苏南通万达港闸店', 'address' => '江苏省南通市港闸区深南路9号1层DW精品店', 'lat' => 32.0388657, 'lon' => 120.8329311);
		$list[] = array('name' => '沈阳大悦城POP', 'address' => '辽宁省沈阳市大东区小东街道小东路10-3号沈阳大悦城C馆1层DW精品店', 'lat' => 41.8029486, 'lon' => 123.4715819);
		$list[] = array('name' => '洛阳万达POP', 'address' => '河南省洛阳市涧西区辽宁路168号1层2号门 DW精品店', 'lat' => 34.654316, 'lon' => 112.409144);
		$list[] = array('name' => '安徽蚌埠万达', 'address' => '安徽省蚌埠市工农路万达广场1层1号门 DW精品店', 'lat' => 32.9259183, 'lon' => 117.3589162);
		$list[] = array('name' => '昆山金鹰POP', 'address' => '南京市栖霞区学津路金鹰二期1层 DW精品店', 'lat' => 32.0987559, 'lon' => 118.9229366);
        $list[] = array('name' => '丹阳八佰伴POP', 'address' => '江苏省丹阳市新民中路 2号丹阳八佰伴1层DW精品店', 'lat' => 31.9979256820, 'lon' => 119.5795459485);
        $list[] = array('name' => '沈阳太原街万达', 'address' => '辽宁省沈阳市和平区太原街16号万达广场1层DW精品店', 'lat' => 41.7881939786, 'lon' => 123.3673192341);
        $list[] = array('name' => '上海七宝万科', 'address' => '上海市闵行区漕宝路3366号七宝万科广场1层K-B-DW精品店', 'lat' => 31.1603584250, 'lon' => 121.3579096765);
        $list[] = array('name' => '烟台大悦城', 'address' => '山东省烟台市芝罘区北马路150号大悦城1层17号DW精品店', 'lat' => 37.5512412906, 'lon' => 121.3938350190);
        $list[] = array('name' => '上海静安大融城', 'address' => '上海市静安区沪太路1197号大融城1层DW精品店', 'lat' => 31.2842490778, 'lon' => 121.4344183221);
        $list[] = array('name' => '上海中信泰富', 'address' => '上海市静安区南京西路1168号中心泰富广场B1层DW精品店', 'lat' => 31.2510774981, 'lon' => 121.4864217243);
        $list[] = array('name' => '广州K11', 'address' => '广东省广州市天河区珠江东路6号周大福金融中心K11 B1层DW精品店', 'lat' => 23.1236134436, 'lon' => 113.3327478962);
        $list[] = array('name' => '苏州石路国际店', 'address' => '江苏省苏州市姑苏区石路步行街18号石路国际1层DW精品店', 'lat' => 31.3183140349, 'lon' => 120.6084674386);
        $list[] = array('name' => '长沙乐和城', 'address' => '湖南省长沙市芙蓉区定王台街道黄兴中路188号乐和城购物中心1层K106 DW精品店', 'lat' => 28.2044398033, 'lon' => 112.9832720067);
        $list[] = array('name' => '北京东直门来福士', 'address' => '北京市东城区东直门外大街1号北京来福士购物中心0层DW精品店', 'lat' => 39.946065, 'lon' => 116.438911);
        $list[] = array('name' => '广西柳州工贸', 'address' => '广西壮族自治区柳州市城中区龙城路2号工贸大厦首层DW专精品店', 'lat' => 24.3184858871, 'lon' => 109.4182214228);
        $list[] = array('name' => '东莞常平百花时代POP', 'address' => '广东省东莞市常平镇市场路1号百花时代1层DW精品店', 'lat' => 22.9804295757, 'lon' => 114.0005482936);
        $list[] = array('name' => '河南焦作万达广场', 'address' => '河南省焦作市解放区民主南路988号万达广场1层DW精品店', 'lat' => 35.2108982728, 'lon' => 113.2448915116);
        $list[] = array('name' => '北京双井富力城', 'address' => '北京市朝阳区东三环中路65号楼1层102内102号DW精品店', 'lat' => 39.8998873221, 'lon' => 116.4672646458);
        $list[] = array('name' => '安徽安庆吾悦广场', 'address' => '安徽省安庆市迎江区菱湖南路1号新城吾悦广场1层DW精品店', 'lat' => 30.5197517507, 'lon' => 117.0818466402);
        $list[] = array('name' => '天津河东万达', 'address' => '天津市河东区津滨大道53号万达广场1层DW精品店', 'lat' => 39.1310859674, 'lon' => 117.2596168243);
        $list[] = array('name' => '福清万达', 'address' => '福建省福清市清昌大道105号福清万达1层DW精品店', 'lat' => 25.7238974902, 'lon' => 119.3550779346);
        $list[] = array('name' => '上海世茂广场', 'address' => '上海市黄浦区南京东路819号世贸广场3层DW精品店', 'lat' => 31.2403124453, 'lon' => 121.4823216392);
        $list[] = array('name' => '烟台芝罘万达', 'address' => '山东省烟台市芝罘区西关南路万达广场1层DW精品店', 'lat' => 37.5418850912, 'lon' => 121.3992445248);
        $list[] = array('name' => '南京溧水万达', 'address' => '江苏省南京市溧水区崇文路与秦淮大道交汇处万达广场1层DW精品店', 'lat' => 31.9939617146, 'lon' => 118.8829821123);
        $list[] = array('name' => '北京崇文门新世界POP', 'address' => '北京市东城区崇文门新世界2期2层DW精品店', 'lat' => 39.9027688438, 'lon' => 116.4242452929);
        $list[] = array('name' => '昆明南亚风情第一城', 'address' => '云南省昆明市西山区滇池路南亚风情第壹城mall1层DW精品店', 'lat' => 25.0159188053, 'lon' => 102.6924957570);
        $list[] = array('name' => '长沙王府井', 'address' => '湖南省长沙市黄兴中路27号王府井百货2层DW精品店', 'lat' => 28.1983569101, 'lon' => 112.9823481152);
        $list[] = array('name' => '南通万象城', 'address' => '江苏省南通市港闸区北大街与永和路交叉口万象城1层DW精品店', 'lat' => 32.0552045738, 'lon' => 120.8643517879);
        $list[] = array('name' => '武汉销品茂', 'address' => '湖北省武汉市徐东路销品茂购物中心1层DW精品店', 'lat' => 30.5942737281, 'lon' => 114.3493222355);
        $list[] = array('name' => '山东东营万达', 'address' => '山东省东营市东营区北一路730号万达广场1层DW精品店', 'lat' => 37.4634987941, 'lon' => 118.5485150186);
        $list[] = array('name' => '镇江八佰伴', 'address' => '江苏省镇江市京口区中山东路288号八佰伴1层DW精品店', 'lat' => 32.2104465714, 'lon' => 119.4551883550);
        $list[] = array('name' => '福州世欧广场', 'address' => '福建省福州市晋安区世欧王庄广场北区1层DW精品店', 'lat' => 26.0773837132, 'lon' => 119.3336557762);
        $list[] = array('name' => '深圳雅宝COCOPARK', 'address' => '广东省深圳市龙岗区坂田街道 雅宝路COCO PARK 1层DW精品店', 'lat' => 22.6095869411, 'lon' => 114.0654897371);
        $list[] = array('name' => '广州天河城', 'address' => '广东省广州市天河区天河路208号天河城购物中心2层K272铺DW精品店', 'lat' => 23.1379810671, 'lon' => 113.3295329105);
        $list[] = array('name' => '深圳万象天地POP', 'address' => '深圳市南山区粤海街道深南大道9668号华润城万象天地B1层DW精品店', 'lat' => 22.5476558708, 'lon' => 113.9623954764);
        $list[] = array('name' => '北京荟聚', 'address' => '北京市大兴区欣宁街15号荟聚购物中心1层DW精品店', 'lat' => 39.7937620943, 'lon' => 116.3328966545);
        $list[] = array('name' => '武汉群光', 'address' => '湖北省武汉市洪山区珞喻路6号群光广场二期2层DW精品店', 'lat' => 30.5314113114, 'lon' => 114.3617930573);
        $list[] = array('name' => '安徽马鞍山金鹰', 'address' => '安徽省马鞍山市花山区湖南西路108号金鹰天地广场1层DW精品店', 'lat' => 31.6902836806, 'lon' => 118.5164727674);
        $list[] = array('name' => '武汉银泰创意城', 'address' => '湖北省武汉市洪山区珞瑜路35号银泰创意城1层DW精品店', 'lat' => 30.5328921066, 'lon' => 114.3622939798);
        $list[] = array('name' => '宜昌国贸店', 'address' => '湖北省宜昌市西陵区东山大道106号国贸大厦1层DW精品店', 'lat' => 30.7084152144, 'lon' => 111.3030438017);
        $list[] = array('name' => '上海中信泰富万达', 'address' => '上海市嘉定区胜幸路426号中信泰富万达广场1层107-3 DW精品店', 'lat' => 31.3362442692, 'lon' => 121.2618131001);
        $list[] = array('name' => '武汉江汉路步行街新世界', 'address' => '湖北省武汉市江汉区江汉路步行街118号新世界百货（时尚广场店）1层DW精品店', 'lat' => 30.6010852192, 'lon' => 114.2766341834);
        $list[] = array('name' => '深圳龙岗万科', 'address' => '广东省深圳市龙岗区龙翔大道7188号万科广场1层DW精品店', 'lat' => 22.7205979513, 'lon' => 114.2500120438);
        $list[] = array('name' => '上海虹口龙之梦', 'address' => '上海市虹口区西江湾路388号龙之梦购物中心（虹口店）1层DW精品店', 'lat' => 31.2765513227, 'lon' => 121.4853178644);
        $list[] = array('name' => '成都锦华万达', 'address' => '四川省成都市锦江区锦华路一段68号锦华万达广场1层DW精品店', 'lat' => 30.6269102330, 'lon' => 104.1043731037);
        $list[] = array('name' => '深圳卓悦汇', 'address' => '深圳市福田区上梅林中康路卓悦汇购物中心1层DW精品店', 'lat' => 22.5750529766, 'lon' => 114.0669345679);
        $list[] = array('name' => '开封大商新玛特', 'address' => '河南省开封市西门大街388号大商新玛特总店1层DW精品店', 'lat' => 34.8065399532, 'lon' => 114.3561163487);
		return $list;
    }


}
