<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>Admin</title>
</head>
<body>

<div style="width: 800px; margin: 10px auto;">
	<h1><?php echo $date;?></h1>

	<table border="1" style="width:100%;border-collapse: collapse;">
		<tr>
			<th width="80%" align="center">门店</th>
			<th align="center">领券</th>
		</tr>
		<?php foreach($list as $store): ?>
		<tr>
			<td><?php echo $store['name'];?></td>
			<td align="center"><?php echo $store['count']; ?></td>
		</tr>
		<?php endforeach;?>
	</table>

	<br/>
	<a href="<?php echo site_url('admin');?>">返回</a>
</div>
</body>
</html>
