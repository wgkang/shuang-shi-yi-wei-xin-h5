<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>Admin</title>
</head>
<body>

<div style="width: 800px; margin: 10px auto;">
	<form method="post" action="<?php echo site_url('admin/login');?>">
		<input type="password" name="token"/>
		<input type="submit" value="Submit"/>
	</form>
	<?php if(!empty($error)): ?>
	<div style="color: red; font-weight: bold;"><?php echo $error;?></div>
	<?php endif;?>
</div>

</body>
</html>
