FROM ubuntu:16.04

RUN apt-get update -y && \
    apt-get install -y php7.0-fpm php7.0-curl nginx vim-nox tcpdump traceroute curl && \
    apt-get clean

COPY conf/sysctl.conf /etc/sysctl.conf
COPY conf/www.conf /etc/php/7.0/fpm/pool.d/www.conf
COPY conf/default /etc/nginx/sites-available/default
COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY src/ /var/www/html/

RUN chmod 777 /var/www/html/application/logs/

VOLUME ["/var/www/html/application/logs/"]

EXPOSE 80

CMD /etc/init.d/php7.0-fpm start && /usr/sbin/nginx -g "daemon off; error_log /dev/stderr info;"
