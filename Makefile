.PHONY: default

NAMESPACE = 885839867296.dkr.ecr.cn-north-1.amazonaws.com.cn
PACKAGE_NAME = singlesday
PACKAGE_VERSION = $(shell git rev-parse HEAD)

build:
	docker build -t $(NAMESPACE)/$(PACKAGE_NAME):$(PACKAGE_VERSION) .

push:
	docker push $(NAMESPACE)/$(PACKAGE_NAME):latest
	docker push $(NAMESPACE)/$(PACKAGE_NAME):$(PACKAGE_VERSION)

deploy-prod:
	cd deploy && bash deploy.sh PROD $(PACKAGE_HASH)

default: build