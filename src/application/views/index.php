<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="default" name="apple-mobile-web-app-status-bar-style">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, minimal-ui" name="viewport">
    <title>DANIEL WELLINGTON | 线下线上同步狂欢</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/app.css"/>
    <script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.slideunlock.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/app.js"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?35b12aadf6ef0979a7e4e875f5ab838d";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</head>
<body>
<div id="container">
    <div id="page-unlock" class="page">
        <div class='page-hand-sands' id='page-hand-sands-1' style="display: none;">
           <div class="logo" style="display: none;" id="unlock-logo"></div>
           <div id="unlock-text" style="display: none;">线上线下狂欢</div>
           <div class="icon-key" id="unlock-key" style="display: none;">
                <img src="<?php echo base_url();?>assets/images/images/W1.png" id="w1">
                <img src="<?php echo base_url();?>assets/images/images/W2.png" id="w2">
            </div> 
            <div class="btn" id="btn" style="display: none;">
                <img src="<?php echo base_url();?>assets/images/images/Light.png" id="light">
                <a class="unlock" id="unlock-btn">畅享狂欢</a>
                <a id="unlock-btn-1"></a>
            </div>
        </div>
    </div>
    <div id="page-hand" class="page"  style="display: none;">
     <div class='page-hand-sands' id="page-hand-sands-2" style="display: none;">
           <div class="logo"></div>
            <div id="hend-text" style="display: none;">你获得2张狂欢券</div>
            <div id='hand-haed' style="display: none;"></div>
            <div id="hend-body">
                <div id='card' style="display: none;">
                    <div id="card-1" style="display: none;">
                        <span id="card-head">限时优惠券</span>
                    </div>
                    <div id="card-2" style="display: none;">
                        <div class="card-2-main">
                            <div class="card-2-left">
                                <span>50</span>
                            </div>
                            <div id="card-2-right">
                                <div class="card-2-right-body">
                                    <span class="card-2-right-1">YUAN</span>
                                    <span class="card-2-right-2">优惠券</span>
                                    <span class="card-2-right-1">满 300 立减</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="card-3" style="display: none;">
                        <div class="card-2-main" id="card-3-main">
                            <div class="card-2-left">
                                <span>300</span>
                            </div>
                            <div id="card-2-right">
                                <span class="card-2-right-1">YUAN</span>
                                <span class="card-2-right-2">优惠券</span>
                                <span class="card-2-right-1">满 1000 立减</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-4"  id="card-4" style="display: none;">
                        <div class="card-2-main">
                            <div class="card-4-left">
                                <span class="card-4-left-1">11.9-11.11</span>
                                <span class="card-4-left-2">三天内使用</span>
                            </div>
                            <div class="card-4-right" id="btn-claim">
                                <a>领取惊喜</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="page-box" class="page"  style="display: none;">
        <div class="logo"></div>
        <div class="main-text" id="box-text">
			 <span class="main-text-1" style="display: none;">请到微信卡包查看优惠券</span>
			 <span class="main-text-2" style="display: none;">注册会员更可累计积分奖励，尊享专属权益</span>
        </div>
        <a class="btn-open" id="btn-open" style="display: none;">加入DW会员俱乐部</a>
    </div>
    <a class="btn-music" id="btn-music"></a>
</div>

<div style="display:none;">
    <audio controls="false" loop="loop" id="audio-item">
        <source src="<?php echo base_url();?>assets/audio/music.mp3" />
    </audio>
</div>

<?php if(isset($signature)):?>
<script type="text/javascript">

wx.config({
    debug: <?php echo ENVIRONMENT == 'development' ? 'true' : 'false';?>, 
    appId: '<?php echo WX_APPID?>', 
    timestamp: '<?php echo $signature['timestamp']?>', 
    nonceStr: '<?php echo $signature['nonce']?>',
    signature: '<?php echo $signature['signature']?>',
    jsApiList: ['getLocation', 'addCard', 'openCard', 'onMenuShareAppMessage','onMenuShareTimeline']
});

var gps = '';
var shareUrl = '<?php echo site_url('/');?>';
var shareTitle = 'DANIEL WELLINGTON | 线下线上同步狂欢';
var shareDesc = '精致生活，畅享狂欢';
var shareIcon = '<?php echo base_url();?>assets/images/images/WechatIMG89.jpeg';

wx.ready(function(){
    wx.getLocation({
        success: function (res) {
            gps = '' + res.latitude + "," + res.longitude;
        },
        cancel: function (res) {
            // user cancelled
        }
    });
    DS.ready(function(){
        wx.onMenuShareAppMessage({
            title: shareTitle,
            desc: shareDesc, 
            link: DS.linkChange(shareUrl), 
            imgUrl: shareIcon, 
            type: 'link', 
            dataUrl: '', 
            success: function () {
                DS.sendRepost('appMessage');
            },
            cancel: function () { 
            }
        });
        wx.onMenuShareTimeline({
            title: shareTitle, 
            link: DS.linkChange(shareUrl), 
            imgUrl: shareIcon, 
            success: function () { 
                DS.sendRepost('timeline');
            },
            cancel: function () { 
            }
        });
    });
});

document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
    // 发送给好友
    WeixinJSBridge.on('menu:share:appmessage', function (argv) {
        WeixinJSBridge.invoke('sendAppMessage', { 
            "img_url": shareIcon,
            "img_width": "64",
            "img_height": "64",
            "link": shareUrl,
            "desc": shareDesc,
            "title": shareTitle
        }, function (res) {
            _report('send_msg', res.err_msg);
        })
    });

    // 分享到朋友圈
    WeixinJSBridge.on('menu:share:timeline', function (argv) {
        WeixinJSBridge.invoke('shareTimeline', {
            "img_url": shareIcon,
            "img_width": "64",
            "img_height": "64",
            "link": shareUrl,
            "desc": shareDesc,
            "title": shareTitle
        }, function (res) {
            _report('timeline', res.err_msg);               
        });
    });     
}, false);

$(function(){
    var screenWidth = window.screen.width
    if (screenWidth > 1024) return

    var rate = 625 / 375
    var fs = document.body.clientWidth * rate
    document.querySelector('html').style.fontSize = fs + '%'

    var height = $(window).height()
    var width = $(window).width()
    
    $('#container').css('width', width)
    $('#unlock-key').css('width', width)
    $('#page-hand-sands-1').fadeToggle(1000,function(){
        $('#unlock-logo').fadeToggle(1000, function () {
        $('#unlock-text').fadeToggle(500, function () {
            $('#unlock-key').fadeToggle(1000, function () {
                $('.btn').fadeToggle(800)
            })
        })
        })
    })


    $('#btn').click(function () {
        $('#btn').hide()
        $('#page-hand').show()
        $('#page-hand-sands-2').fadeToggle(1000,function(){
            $('#hend-text').fadeToggle(500,function () {
                $('#hand-haed').fadeToggle(500)
                $('#card').fadeToggle(500,function () {
                    $('#card-1').fadeToggle(500,function () {
                        $('#card-2').fadeToggle(500,function () {
                            $('#card-3').fadeToggle(500, function () {
                                $('#card-4').fadeToggle(500)
                            })
                        })
                    })
                })
            })
        })


    })

    $('#btn-music').click(function(){
        $(this).toggleClass('btn-music-on');
        var audio = document.getElementById('audio-item');
        if($(this).hasClass('btn-music-on')){
            audio.play();
        }
        else{
            audio.pause();
        }
    })

    $('#btn-open').click(function(){
        window.location.href="https://club.danielwellington.cn/member/account/register"
    });

    $('#btn-claim').click(function(){

        wx.addCard({
            cardList: [
                <?php foreach($cardList as $i => $card): ?>
                    <?php if($i > 0): ?>,<?php endif;?>
                {
                         
                    cardId: '<?php echo $card['cardId'];?>',
                    cardExt: '{"timestamp": "<?php echo $card['timestamp'];?>", "signature":"<?php echo $card['signature'];?>"}'
                }
                <?php endforeach;?>
            ],
            success: function (res) {
                $('#page-box').show()
                $('.main-text-1').fadeToggle(1000, function () {
                    $('.main-text-2').fadeToggle(1000, function () {
                        $('#btn-open').fadeToggle(500)
                    })
                })

                var idList = '';
                var codeList = '';
                for(var i = 0 ; i < res.cardList.length; i ++){
                    if(i > 0){
                        idList += ',';
                        codeList += ',';
                    }
                    idList += res.cardList[i].cardId;
                    codeList += res.cardList[i].code;
                }
                idList = encodeURIComponent(idList);
                codeList = encodeURIComponent(codeList);
                gps = encodeURIComponent(gps);
                $.get('<?php echo site_url('user/record');?>/' + idList + "/" + codeList + "/" + gps, function(resp){
                    <?php if(ENVIRONMENT == 'development'):?>
                    alert(resp);
                    <?php endif;?>
                });
  
                        },
            cancel: function (res) {
                console.log(res)
            }
        });         
    });
});

</script>
<?php endif; ?>
</body>
</html>
