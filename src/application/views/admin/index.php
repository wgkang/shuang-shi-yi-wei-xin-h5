<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>Admin</title>
</head>
<body>

<div style="width: 800px; margin: 10px auto;">
	<h1>报表</h1>

	<table border="1" style="width:100%;border-collapse: collapse;">
		<tr>
			<th align="center">日期</th>
		</tr>
		<?php $date = date('Y-m-d');?>
		<?php while($date > '2018-10-12'): ?>
		<?php $date = date('Y-m-d', strtotime('-1 day', strtotime($date)));?>
		<tr>
			<td><a href="<?php echo site_url('admin/report/'.$date);?>"><?php echo $date;?></a></td>
		</tr>
		<?php endwhile;?>
	</table>
</div>
</body>
</html>
