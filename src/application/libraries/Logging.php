<?php

class Logging{
	public function log_error($message){
		$this->_log('ERROR ' . $message);
	}

	public function log_debug($message){
		if(ENVIRONMENT == 'development'){
			$this->_log('DEBUG ' . $message);
		}
	}

	public function _log($message){ 
		$filename = date('Ymd').'.log';
		$file = APPPATH . 'logs' . DIRECTORY_SEPARATOR . $filename;
		$line = '[' . date('Y-m-d H:i:s') . ']' . $message . "\n";
		file_put_contents($file, $line, FILE_APPEND);	
	}
}
