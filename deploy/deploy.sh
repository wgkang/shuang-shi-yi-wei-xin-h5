#/bin/bash -e
# Script for deploy of ECS clusters

#####
# Configuration
#####

# Docker
DOCKER_IMAGE_NAMESPACE=885839867296.dkr.ecr.cn-north-1.amazonaws.com.cn
if [ "$2" == "" ]; then
    echo "ERROR: Missing container hash value"
    exit 1
else
    DOCKER_IMAGE_VERSION=$2
fi

################## Functions ##################

update_task_def() {
	# Decrypt secrets
	ADMIN_TOKEN=$(/usr/bin/dw_decrypt $ADMIN_TOKEN_CIPHERTEXT)
	WX_SECRET=$(/usr/bin/dw_decrypt $WX_SECRET_CIPHERTEXT)

	# Replace placeholders in template dwtest
	sed -e "s/&ADMIN_TOKEN&/$ADMIN_TOKEN/g" \
        -e "s/&WX_SECRET&/$WX_SECRET/g" \
		-e "s|&DOCKER_IMAGE&|$DOCKER_IMAGE_NAMESPACE/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_VERSION|g" $TEMPLATE_FILE > $TEMPORARY_TASKDEF_FILE 

	# register taskdefinition
	REVISION=$(aws ecs register-task-definition --cli-input-json file://$TEMPORARY_TASKDEF_FILE --query 'taskDefinition.revision' --output text --region $REGION)
	echo "$TASKDEF taskdefinition updated to revision $REVISION"
}

update_ecs_service() {
	echo "Starting update of $CLUSTER..."
	aws ecs update-service --cluster $CLUSTER --service $SERVICE --task-definition $TASKDEF --region $REGION ${PROFILE} > tempfiles/update_service_dwadmintest.log
	OLDTASKDEFARN=$(aws ecs list-tasks --cluster $CLUSTER --family $TASKDEF --region $REGION --query 'taskArns[0]' --output text ${PROFILE})
	if [ "${OLDTASKDEFARN}" != "None" ]; then
		aws ecs stop-task --cluster $CLUSTER --task $OLDTASKDEFARN --reason Deploy --region $REGION ${PROFILE} > tempfiles/stop-task.log
		while [ $(aws ecs describe-tasks --cluster $CLUSTER --tasks $OLDTASKDEFARN --query 'tasks[0].lastStatus' --output text --region $REGION ${PROFILE}) != "STOPPED" ]; do sleep 1; echo "Stopping task $OLDTASKDEFARN"; done
		echo "$OLDTASKDEFARN stopped..."
		sleep 1
	fi
	I=0
	while [ ${I} -lt 30 ]; do
		NEWTASKDEFARN=$(aws ecs list-tasks --cluster $CLUSTER --family $TASKDEF --query 'taskArns[0]' --output text --region $REGION ${PROFILE})
		if [ "${NEWTASKDEFARN}" != "" ] && [ "${NEWTASKDEFARN}" != "None" ]; then
			break
		fi
		let I=I+1
		sleep 1
	done
	if [ "${NEWTASKDEFARN}" == "None" ]; then
		echo "Could not find the new task ARN, something went horribly wrong.. exiting"
		exit 1
	fi
	
	while [ $(aws ecs describe-tasks --cluster $CLUSTER --tasks $NEWTASKDEFARN --query 'tasks[0].lastStatus' --output text --region $REGION ${PROFILE}) != "RUNNING" ]; do sleep 1; echo "Start task $NEWTASKDEFARN"; done
	echo "Update done..."
}

deploy() {
	update_task_def
	update_ecs_service
}



################## Main ##################

# Temp directory
mkdir -p tempfiles
case $1 in
    "TEST")
		. test.env
		;;
	"PROD")
		. prod.env
		;;
	*)
        echo "Usage: $0 [TEST|PROD] PACKAGE_HASH"
		exit 1
esac

if [ -z "${REGION}" ]; then
	REGION=cn-north-1
fi

deploy

rm -rf tempfiles
