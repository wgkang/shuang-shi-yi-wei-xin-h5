<?php
require_once(APPPATH . 'libraries/aws/aws-autoloader.php');
use Aws\DynamoDb\DynamoDbClient;

class User_model extends CI_Model{
	public function __construct(){
		parent::__construct();		
		$this->load->library('logging');
	}

	public function addItem($openId, $cardId, $cardCode, $gps = '')
	{
		$id = uniqid(SERVER_ID);
		$time = time();
		if(empty($openId)) $openId = '-';
		if(empty($cardId)) $cardId = '-';
		if(empty($cardCode)) $cardCode = '-';
		if(empty($gps)) $gps = ',';

		try{
			$client = $this->_getClient();
			$result = $client->putItem(array(
			    'TableName' => 'singlesday',
			    'Item' => array(
			        'id' => array('S' => $id),
			        'userId' => array('S' => strval($openId)),
			        'cardCode' => array('S' => strval($cardCode)),
			        'cardId' => array('S' => strval($cardId)),
			        'cardExt' => array('S' => 'ext'),
			        'gps' => array('S' => strval($gps)),
			        'timestamp' => array('N' => strval($time)),
			    )
			));
			return true;
		} catch(Exception $e){
			$this->logging->log_error('AWS Exception: ' . $e->getMessage());
			return false;
		}
	}

	public function countByOpenId($openId)
	{
		$client = $this->_getClient();
		$scan = $client->scan(array(
		    'TableName' => 'singlesday',
		    'Count' => true,
		    'Limit' => 2,
		    'ScanFilter' => array(
		        'userId' => array(
		            'AttributeValueList' => array(
		                array('S' => $openId)
		            ),
		            'ComparisonOperator' => 'EQ'
		        )
		    )
		));
		return $scan['Count'];
	}

	public function listLocationsByDate($date = NULL)
	{
		$file = APPPATH . 'logs' . DIRECTORY_SEPARATOR . 'report-'.$date.'.log';
		if(file_exists($file)){
			// save to file
			return json_decode(file_get_contents($file), TRUE);
		}

		$client = $this->_getClient();
		$list = array();

		try{
			$startKey = array();
			do {
			    $args = array('TableName' => 'singlesday') + $startKey;
			    $result = $client->scan($args);

			    foreach ($result['Items'] as $item) {
			        // Do something with the $item
			        $time = $item['timestamp']['N'];
			        if($date == NULL || date('Y-m-d', $time) == $date){
			        	$gps = trim($item['gps']['S']);
			        	if(!empty($gps) && $gps != ','){
			        		$list[] = $gps;
			        	}
			        }
			    }
			    $startKey['ExclusiveStartKey'] = $result['LastEvaluatedKey'];
			} while ($startKey['ExclusiveStartKey']);
			if($date != NULL && $date != date('Y-m-d')){
				file_put_contents($file, json_encode($list));
			}
			return $list;
		}
		catch(Exception $e){
			$this->logging->log_error('AWS Exception: ' . $e->getMessage());
			return false;
		}
	}

	private function _getClient()
	{	
		$client = DynamoDbClient::factory(array(
			'version' => 'latest',
		    'region'  => 'cn-north-1'
		));
		return $client;
	}
}

	